const http = require('http');

const { networkInterfaces } = require('os');

PORT = process.env.APP_PORT;
if (PORT == null || PORT === '') PORT = 9999;

APP_NAME = process.env.APP_NAME;
if (APP_NAME == null || APP_NAME === '') APP_NAME = 'default-name';

const APP_VERSION = '2.2';

const requestListener = function (req, res) {

  let data = '';
  req.on('data', chunk => {
    data += chunk;
  });
  req.on('end', () => {
    if (data === '') data = null;

    const nets = networkInterfaces();
    const results = {};

    for (const name of Object.keys(nets)) {
      for (const net of nets[name]) {
        if (!results[name]) {
          results[name] = [];
        }
        results[name].push(net.address);
      }
    }

    let r = {
      'app_version': APP_VERSION,
      'current_epoch': Date.now(),
      'APP_NAME': APP_NAME,
      'request_method': req.method,
      'request_url': req.url,
      'request_headers': req.headers,
      'request_body': data,
      'server_network_info': results
    };

    let responseCode = 200;

    if (req.headers.hasOwnProperty('x-debug-api-response-code')) {
      let responseCodeHeader = req.headers['x-debug-api-response-code'];
      responseCode = parseInt(responseCodeHeader);
      if (responseCode > 599 || responseCode < 100 || isNaN(responseCode)) {
        r['server_message'] = `Specified response code ${responseCodeHeader} is out of range (100~599) or is not a number. Defaulting to 200.`;
        responseCode = 200;
      }
    }

    let responseString = JSON.stringify(r, null, 2);

    console.log(responseString);

    res.writeHead(responseCode, { 'Content-Type': 'application/json' });
    res.end(responseString);
  });
};

const server = http.createServer(requestListener);
console.log(`listening on port ${PORT}`);
server.listen(PORT);
