# debug-api


[![pipeline status](https://gitlab.com/conwayok/debug-api/badges/main/pipeline.svg)](https://gitlab.com/conwayok/debug-api/-/commits/main)

this is a simple node.js server app for debugging purposes

sample response from GET http://localhost:9999/hello/world on my machine:

```json
{
  "current_epoch": 1614087322167,
  "APP_NAME": "default-name",
  "request_method": "GET",
  "request_url": "/hello/world",
  "request_headers": {
    "host": "localhost:9999",
    "connection": "keep-alive",
    "dnt": "1",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "sec-fetch-site": "none",
    "sec-fetch-mode": "navigate",
    "sec-fetch-user": "?1",
    "sec-fetch-dest": "document",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "en-US,en;q=0.9"
  },
  "request_body": null,
  "server_network_info": {
    "Ethernet": [
      "fe80::c885:6569:5447:50f9",
      "192.168.0.154"
    ],
    "VirtualBox Host-Only Network": [
      "fe80::608c:433f:d723:43ba",
      "192.168.56.1"
    ],
    "Ethernet 3": [
      "fe80::5dd3:22ae:ff50:e8b9",
      "192.168.235.2"
    ],
    "Loopback Pseudo-Interface 1": [
      "::1",
      "127.0.0.1"
    ],
    "vEthernet (Default Switch)": [
      "fe80::6da5:201f:2d29:b0ed",
      "172.17.77.33"
    ]
  }
}
```
