FROM node:8-alpine
ADD server.js /server.js
ENTRYPOINT ["node", "server.js"]
